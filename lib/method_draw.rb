require "method_draw/engine"
require "method_draw/method_draw"
require "method_draw/method_draw_tag"

module MethodDraw
  ASSET_FILES = %w(method_draw.css method_draw.js method_draw_embed.js method-draw-js/canvg/canvg.js method-draw-js/canvg/rgbcolor.js method-draw-js/extensions/ext-*.js)

  METHOD_DRAW_OPTIONS = [:width, :height, :bkgd_color, :bkgd_url, :canvas_width, :canvas_height, :canvas_expansion, :hide_rulers, :hide_menu, :hide_image_tool, :show_layers, :url, :extensions]
  TAG_OPTIONS = [:id, :value, :index]
  OPTIONS = METHOD_DRAW_OPTIONS + TAG_OPTIONS

  ActiveSupport.on_load(:before_initialize) do
    Rails.configuration.assets.precompile += ASSET_FILES
  end
end

