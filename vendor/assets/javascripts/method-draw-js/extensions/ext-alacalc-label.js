// /*
//  * ext-alacalc-label.js
//  *
//  * Copyright(c) 2014 Simon Funke
//  *
//  */

// /*
//     This is a extension for method-draw to be used in alacalc.
// */

// function GetURLParameter(sParam)
// {
//     var sPageURL = window.location.search.substring(1);
//     var sURLVariables = sPageURL.split('&');
//     for (var i = 0; i < sURLVariables.length; i++) 
//     {
//         var sParameterName = sURLVariables[i].split('=');
//         if (sParameterName[0] == sParam) 
//         {
//             return sParameterName[1];
//         }
//     }
// }

// methodDraw.addExtension("Add food label", function() {'use strict';

//         var createFoodLabel = function() {
//            // The coordinates for the label position
//            var x = 0;
//            var y = 0;

//            var labelDesign = $("#alacalcLabelType").val();
//            var RecipeID = $("#alacalcRecipeID").val();            
//            var locale = GetURLParameter("locale");

//            svg_url = "/api/v1/recipes/"+RecipeID+"/label.svg?design="+labelDesign+"&locale="+locale; 

//            // Load label svg
//            $.get(svg_url, function(data) {

//                 // Add the svg element to the canvas
//                 svgCanvas.appendSvgString(data);

//                 // Group the imported graphic as they belong together
//                 svgCanvas.groupSelectedElements();

//                 // Move elements to their mouse location
//                 svgCanvas.moveSelectedElements(x, y, false);
//            }, 'text');

//         }

//         var createDocumentElements = function() {
//             /* Creates all document elements for the alacalc plugin */

//             // Create a pop up dialog where the user inputs
//             // label type
//             $('<div id="alacalcLabelPopupDialog" title="Enter label information">' +
//               '<label for="alacalcRecipeID">Recipe:</label>' +
//               '<select id="alacalcRecipeID"></select>' + 
//               '<label for="alacalcLabelType">Food label type:</label>' +
//               '<select id="alacalcLabelType">' +
//                   '<option value="uk_2013">EU style 1</option>' +
//                   '<option value="uk_2013_2">EU style 2</option>' +
//                   '<option value="uk_2013_vertical_2">EU style 2 vertical</option>' +
//                   '<option value="uk_2013_drinks">EU style 1 (drinks)</option>' + 
//                   '<option value="uk_2013_drinks_2">EU style 2 (drinks)</option>' +
//                   '<option value="uk_2013_drinks_vertical_2">EU style 2 vertical (drinks)</option>' +
//                   '<option value="big_8">BIG 8</option>' +
//                   '<option value="nutrition_facts_vertical">US Nutrition facts vertical</option>' +
//                   '<option value="nutrition_facts_vertical_2">US Nutrition facts vertical style 2</option>' +
//                   '<option value="nutrition_facts_vertical_compact">US Nutrition facts compact</option>' + 
//                   '<option value="nutrition_facts_horizontal">US Nutrition facts horizontal</option>' + 
//                   '<option value="nutrition_facts_horizontal_compact">US Nutrition facts horizontal compact</option>' +
//               "</select>" +
//               "</div>").insertAfter("#svg_editor");

//               // Populate dropdown list with recipes
//               //
//               $.getJSON("/api/v1/recipes", function(data) {
//                   $.each( data["recipes"], function( key, recipe ) {
//                       $('#alacalcRecipeID').append($('<option>', {value:recipe["id"], text:recipe["name"]}));
//                   });
//               });

//               // Create the dialog window
//               // (closed by default)
//               $('#alacalcLabelPopupDialog').dialog({
//                  modal: true,
//                  autoOpen: false,
//                  buttons: {
//                              'Cancel': function() {
//                                    $(this).dialog('close');
//                              },
//                              'Accept': function() {
//                                  // Load the food label and add it to
//                                  // the svg canvas
//                                  createFoodLabel();
//                                  $(this).dialog('close');
//                              }
//                           }
//                 });

//         }

//         // Create the document elements
//         createDocumentElements();

//         return {
//             name: "Alacalc label",

//             // The extension icon
//             svgicons: "extensions/alacalc-label-icon.xml",

//             // Multiple buttons can be added in this array
//             buttons: [{
//                 // Must match the icon ID in alacalc-label-icon.xml
//                 id: "alacalc_label",

//                 // This indicates that the button will be added to the "mode"
//                 // button panel on the left side
//                 type: "mode",

//                 // Tooltip text
//                 title: "Add food label",

//                 // Events
//                 events: {
//                     // The action taken when the button is clicked on.
//                     // For "mode" buttons, any other button will
//                     // automatically be de-pressed.
//                     'click': function() {

//                         // Ask the user for the barcode information
//                         $('#alacalcLabelPopupDialog').dialog('open');
//                     }
//                 }
//             }]

//         };
// });
