// /*
//  * ext-barcode.js
//  *
//  * Copyright(c) 2014 Simon Funke
//  *
//  */

// /*
// 	This is a extension for method-draw for adding barcodes.
// */

// methodDraw.addExtension("Add barcode", function() {'use strict';

//         var loadBarcodeLibrary = function() {
//             /* Loads the jquery barcode library */
//               $.getScript(bootstrapSrc);
//               $.getScript(barcodeSrc)
//                 .done(function(script, textStatus) {
//                 barcodeLoaded = true;
//                 console.log('Barcode plugin loaded');
//                 return true;
//               })
//                 // If it fails.
//                 .fail(function() {
//                 console.log("Failed loading barcode plugin.");
//                 $.alert("Failed loading barcode plugin. You will not be able to add barcodes.");
//                 return false;
//               });

//         }

//         var createDocumentElements = function() {
//                /* Creates all document elements that the barcode plugin uses */

//               // Here is where barcode creates the barcode
//               $('<div id="barcodeTarget"></div>'
//                 ).insertAfter('#svg_editor').hide();

//               // Also create the pop up dialog where the user inputs
//               // the barcode information
//               $('<div id="barcodePopupDialog" title="Enter barcode information">'
//               + '<label for="barcodeNumber">Barcode number:</label>'
//               + '<input type="text" id="barcodeNumber"/>'
//               + '<div id="barcodeError" style="display:none; color:red">ERROR</div>'
//               + '<p>'
//               + '<label for="barcodeType">Barcode type:</label>'
//               + '<select id="barcodeType">'
//               + '<option value="ean8">EAN-8</option>'
//               + '<option value="ean13">EAN-13</option>'
//               + '<option value="itf14">ITF-14</option>'
//               + '<option value="code39">Code 39</option>'
//               + '<option value="code128">GS1-128 (Code 128)</option>'
//               + '<option value="codabar">Codabar</option>'
//               + '<option value="std25">Standard 2 of 5</option>'
//               + '<option value="int25">Interleaved 2 of 5</option>'
//               + '<option value="msi">MSI Barode</option>'
//               + '</select>'
//               + '</p>'
//               + '</div>').insertAfter('#svg_editor');//.hide();

//               var handleErrors = function() {
//                   var barcodeType = $(document).find('#barcodeType').val();
//                   var barcodeNumber = $(document).find('#barcodeNumber').val();

//                   var msg = "";
//                   if (barcodeType == "itf14" && (barcodeNumber.length < 13 ||  barcodeNumber.length > 14)) {
//                       msg = "Must be 13 or 14 digits.";
//                   }
//                   else if (barcodeType == "ean8" && (barcodeNumber.length < 7 ||  barcodeNumber.length > 8)) {
//                       msg = "Must be 7 or 8 digits.";
//                   }
//                   else if (barcodeType == "ean13" && (barcodeNumber.length < 12 ||  barcodeNumber.length > 13)) {
//                       msg = "Must be 12 or 13 digits.";
//                   }

//                   // Show or hide error messages
//                   if (msg == "") {
//                       $("#barcodeError").hide();
//                   }
//                   else {
//                       $("#barcodeError").text(msg);
//                       $("#barcodeError").show();
//                   }
//               }

//               // Handle error cases and show error messages
//               $("#barcodeType").on('change', function() {
//                   handleErrors();
//               });

//               $("#barcodeNumber").on('input', function() {
//                   handleErrors();
//               });

//               // Create the dialog window
//               // (closed by default)
//               $('#barcodePopupDialog').dialog({
//                  modal: true,
//                  autoOpen: false,
//                  buttons: {
//                              'Cancel': function() {
//                                    $(this).dialog('close');
//                                 },
//                              'Accept': function() {
//                                  // Create the Barcode and add it to
//                                  // the canvas
//                                  var success = createBarcode();
//                                  if (success) {
//                                      // Close the dialog box
//                                      $(this).dialog('close');
//                                  }
//                                  else {
//                                    alert("Invalid barcode number");
//                                  }
//                              }
//                           }
//                 });

//         }

//         var createBarcode = function() {
//            /* Opens a modela dialog asking for the barcode details */

//            var barcodeNumber = $('#barcodeNumber').val();
//            var barcodeType = $('#barcodeType').val();
//            var simplifiedBarcodeNumber = barcodeNumber;

//            if (barcodeType == "code128") {
//                // Remove brackets and spaces from GS1-128 code
//                var simplifiedBarcodeNumber = barcodeNumber.replace(/[(|)| ]/g, '');
//            }

//            if (barcodeType == "itf14") {
//                // ITF-14 is just a standard int25 code
//                barcodeType = "int25";

//                // If the user provided 14
//                // digits, then don't compute
//                // the crc code
//                var crc = (barcodeNumber.length == 13);
//                simplifiedBarcodeNumber = {code: simplifiedBarcodeNumber, crc: crc};
//            }

//            // This is called AFTER the dialog box has been closed
//            // Get the barcode value:
//            $("#textArea").val("");

//            // Generate the barcode svg
//            $("#barcodeTarget").barcode(simplifiedBarcodeNumber, barcodeType, {output: "svg"});

//            // get generated svg
//            var barcodeSVG = $('#barcodeTarget').contents().attr("data");

//            if (barcodeSVG === undefined) {
//                return false;
//            }
//            else {
//                // replace simplified code with use input
//                barcodeSVG = barcodeSVG.replace(simplifiedBarcodeNumber, barcodeNumber);

//                // Remove some unused header text
//                barcodeSVG = barcodeSVG.substring(19);

//                // And group the barcode
//                // (The first > belongs to the <svg ...> tag)
//                barcodeSVG = barcodeSVG.replace(/>/, '><g>');
//                barcodeSVG = barcodeSVG.replace('</svg>', '</g></svg>');

//                // Add the svg element to the canvas
//                svgCanvas.appendSvgString(barcodeSVG);

//                // Group the imported graphic as they belong together
//                svgCanvas.groupSelectedElements();

//                // Move elements to default location
//                var x = 0;
//                var y = 0;

//                svgCanvas.moveSelectedElements(x, y, false);

//                // Reset barcode div
//                $("#barcodeTarget").text("");
//                return true;
//            }
//         };

//         var barcodeSrc = 'lib/jquery-barcode/jquery-barcode.js',
//             barcodeLoaded = false,
//             bootstrapSrc = '/assets/bootstrap.js';

//         // Add the dialog box into the document tree
//         createDocumentElements();

// 		return {

// 			name: "Barcode",

// 			// The extension icon
// 			svgicons: "extensions/barcode-icon.xml",

// 			// Multiple buttons can be added in this array
// 			buttons: [{
// 				// Must match the icon ID in barcode-icon.xml
// 				id: "barcode",

// 				// This indicates that the button will be added to the "mode"
// 				// button panel on the left side
// 				type: "mode",

// 				// Tooltip text
// 				title: "Add barcode",

// 				// Events
// 				events: {
// 					'click': function() {
// 						// The action taken when the button is clicked on.
//                         // Only load the jquery barcode library when needed,
//                         // we don't want to strain Svg-Edit any more.
//                         // From this point on it is very probable that it will
//                         // be needed, so load it.
//                         if (barcodeLoaded === false) {

//                           // Load the jquery barcode library
//                           barcodeLoaded = loadBarcodeLibrary();
//                         }

//                         // Ask the user for the barcode information
//                         $('#barcodePopupDialog').dialog('open');

// 			        }
// 				}
// 			}]
// 		};
// });
