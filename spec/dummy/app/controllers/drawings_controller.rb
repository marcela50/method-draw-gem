class DrawingsController < ApplicationController
  before_filter :get_drawing

  def show
  end

  def method_draw
  end

  def method_draw_tag
  end

  protected

  def get_drawing
    @drawing = Drawing.find(params[:id])
  end
end
