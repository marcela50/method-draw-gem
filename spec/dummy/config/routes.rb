Dummy::Application.routes.draw do
  resources :drawings, :only => [:show, :update] do
    member do
      get 'method_draw'
      get 'method_draw_tag'
    end
  end
end
