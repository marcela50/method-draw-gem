require 'test_helper'

module MethodDraw
  describe MethodDraw do
    before do
      setup_lib_spec
      @view.form_for(@drawing) do |f|
        @form_builder = f
      end
    end

    it 'must add methods to ActionView::Helpers::FormBuilder and ActionView::Base' do
      @form_builder.must_respond_to :method_draw
      @view.must_respond_to :method_draw
    end

    it 'must show method_draw' do
      method_draw = @form_builder.method_draw('svg')
      method_draw.wont_be_nil
      method_draw.wont_be_empty

      method_draw = @view.method_draw('drawing', 'svg')
      method_draw.wont_be_nil
      method_draw.wont_be_empty
    end
  end
end
